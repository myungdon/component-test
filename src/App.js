import './App.css';
import LifecycleExample2 from "./components/LifecycleExample2";
function App() {
  return (
    <div className="App">
        <LifecycleExample2/>
    </div>
  );
}

export default App;
