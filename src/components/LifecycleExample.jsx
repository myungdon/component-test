import React, { Component } from 'react';

class LifecycleExample extends Component {
    // 가장 먼저 실행.
    constructor(props) {
        // super 부모님꺼 가져옮 component 항상 위에 옮.
        super(props);
        // this = Component 뒤 { }, 컴파일 언어가 이렇게 찾아감.
        // state가 없음 정적 언어가 이렇게 작동함.
        // 정적 언어와 동적 언어 구별 기준. 문자열을 넣었을 때 문자만 들어감 - 정적 언어 (타입이 안바뀜. 컴파일)
        // 동적 언어 js python. 타입이 달라도 넣는대로 들어감.
        // 동적 언어에서 클래스를 지원을 함. 전혀 엄격하지 않음. 변수가 없으면 만들어 버린다.
        // 객체
        this.state = {
            count: 0,
        };
    }

    componentDidMount() {
        console.log('Component mounted');
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.count !== prevState.count) {
            console.log('Count updated:', this.state.count);
        }
    }

    componentWillUnmount() {
        console.log('Component will unmount');
    }

    handleIncrement = () => {
        this.setState((prevState) => ({ count: prevState.count + 1 }));
    };

    render() {
        return (
            <div>
                <p>Count: {this.state.count}</p>
                <button onClick={this.handleIncrement}>Increment</button>
            </div>
        );
    }
}

export default LifecycleExample;