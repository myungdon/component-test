import React, {useState} from "react";

function LifecycleExample2() {
    const [count, setCount] = useState(0);

    React.useEffect(() => {
        console.log('Component mounted');
        return () => {
            console.log('Component will unmount');
        };
    }, []);

    React.useEffect(() => {
        console.log('Count updated:', count);
    }, [count]);

    const handleIncremnet = () => {
        setCount((prevCount) => prevCount + 1);
    };

    return (
        <div>
            <p>Count: {count}</p>
            <button onClick={handleIncremnet}>Increment</button>
        </div>
    );
}

export default LifecycleExample2;

// state가 없다